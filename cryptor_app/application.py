from PySide2 import QtWidgets

from cryptor_app.table_model import CryptorTable, EncryptionTable, EncryptionCell
from cryptor_app.view import MainWindow
from cryptor_app.file_handling import load_file, save_file


class Application:

    def __init__(self, args, window_width, window_height):
        self.pyside_app = QtWidgets.QApplication(args)

        self.cryptor_table = CryptorTable()
        self.encryption_table = EncryptionTable(12)
        self.conversion_names = ['UNICODE', 'BINARY', 'CUSTOM', 'HEXADECIMAL', 'OCTAL']

        self.main_window = MainWindow(self, window_width, window_height)

    def model_info(self):
        return self.encryption_table.info()
    
    def cryptor_cell_count(self):
        return len(self.conversion_names)
    
    def cryptor_cell_header(self, column):
        return self.conversion_names[column]

    def cryptor_cell(self, column):
        return self.cryptor_table.data[column]

    def encryption_cell(self, row, column):
        try:
            return self.encryption_table.data[row][column]
        except IndexError:
            return EncryptionCell('', '')

    def update_cryptor_cell(self, column, value):
        self.cryptor_table.update_cell(column, value, self.encryption_table.data)

    def update_encryption_cell(self, row, column, value):
        self.encryption_table.update_cell(row, column, value)

    def load_encryption_table(self, file_path):
        data = load_file(file_path)
        if data:
            self.encryption_table.load_table(data)
        print("Loading encryption table from file '{}'".format(file_path))
    
    def save_encryption_table(self, file_path):
        data = {col.origin: col.substitution for row in self.encryption_table.data for col in row}
        save_file(file_path, data)
        print("Encryption table saved in '{}'".format(file_path))

    def generate_random_encryption_table(self):
        self.encryption_table.random_cell()
        self.main_window.refresh_encryption_table()

    def run(self):
        self.main_window.show()
        return self.pyside_app.exec_()
