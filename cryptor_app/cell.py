class CryptorCell:

    def __init__(self, value):
        self.text = value

    def __repr__(self):
        return "<{0}>".format(self.text)


class EncryptionCell:

    def __init__(self, origin, substitution):
        self.origin = origin
        self.substitution = substitution

    def __repr__(self):
        return "<{0}({1})>".format(self.origin, self.substitution)

