import sys
from cryptor_app.application import Application


if __name__ == '__main__':
    application = Application(sys.argv, 1080, 740)
    sys.exit(application.run())
