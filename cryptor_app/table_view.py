from PySide2 import QtWidgets, QtCore
from cryptor_app.delegate import CryptorDelegate, EncryptionDelegate


class TableView(QtWidgets.QTableView):
    """
    Represents the base class of all table views of the GUI.
    """

    def __init__(self, model, delegate, parent=None):
        QtWidgets.QTableView.__init__(self, parent)
        
        self.model = model
        self.setModel(self.model)
        self.setItemDelegate(delegate)
        self.resizeRowsToContents()
        self.resizeColumnsToContents()
        self.setVerticalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)

        # Make table horizontally stretched and hide vertical header systematically
        self.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        self.verticalHeader().hide()
    
    def update_cell_size(self, row, column):
        self.resizeRowToContents(row)
        self.resizeColumnToContents(column)


class CryptorTableView(TableView):
    """
    Defines the view on the encodings results.
    """

    def __init__(self, model, parent=None):
        TableView.__init__(self, model, CryptorDelegate(), parent)

        self.verticalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)


class EncryptionTableView(TableView):
    """
    Defines the view on the character encryption table for custom encoding.
    """

    def __init__(self, model, parent=None):
        TableView.__init__(self, model, EncryptionDelegate(), parent)

        self.horizontalHeader().hide()
        self.setMouseTracking(True)