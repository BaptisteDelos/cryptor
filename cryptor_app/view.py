from PySide2 import QtWidgets, QtCore
from cryptor_app.table_view import CryptorTableView, EncryptionTableView
from cryptor_app.qmodel import QCryptorModel, QEncryptionModel


class MainWindow(QtWidgets.QWidget):
    """
    Defines the main window of the application.
    """

    def __init__(self, application, width, height, parent=None):
        QtWidgets.QWidget.__init__(self, parent)

        self.application = application
        
        # Declare table views and models
        self.conversion_model = QCryptorModel(application)
        self.encryption_model = QEncryptionModel(application)
        self.conversion_view = CryptorTableView(self.conversion_model)
        self.encryption_view = EncryptionTableView(self.encryption_model)

        # Organize tables with a splitter
        self.splitter = QtWidgets.QSplitter(self)
        self.splitter.setOrientation(QtCore.Qt.Vertical)
        self.splitter.addWidget(self.conversion_view)
        self.splitter.addWidget(self.encryption_view)
        self.splitter.setCollapsible(0, False)
        self.splitter.setCollapsible(1, False)

        # Initial table heights in splitter
        conversion_table_height = self.splitter.size().height() - self.encryption_view.sizeHint().height()
        encryption_table_height = self.encryption_view.sizeHint().height()
        self.splitter.setSizes([conversion_table_height, encryption_table_height])

        # Encryption table display and save widgets
        self.checkbox_show_table = QtWidgets.QCheckBox('Show encryption table')
        self.checkbox_show_table.setChecked(True)

        self.button_load = QtWidgets.QPushButton('Load')
        self.button_save = QtWidgets.QPushButton('Save')
        self.button_random_table = QtWidgets.QPushButton('Generate random table')
        self.button_load.setMaximumWidth(100)
        self.button_save.setMaximumWidth(100)
        self.button_random_table.setMaximumWidth(200)

        # Displace widgets on layout
        self.layout_grid = QtWidgets.QGridLayout()
        self.layout_grid.addWidget(self.splitter, 0, 0, 1, 4)
        self.layout_grid.addWidget(self.checkbox_show_table, 1, 0)
        self.layout_grid.addWidget(self.button_random_table, 1, 1)
        self.layout_grid.addWidget(self.button_load, 1, 2)
        self.layout_grid.addWidget(self.button_save, 1, 3)
        self.setLayout(self.layout_grid)

        self.resize(width, height)

        # Set up event handlers
        self.checkbox_show_table.clicked.connect(self._on_checkbox_show_clicked)
        self.button_load.clicked.connect(self._on_button_load_clicked)
        self.button_save.clicked.connect(self._on_button_save_clicked)
        self.button_random_table.clicked.connect(self._on_button_random_clicked)
    
    def refresh_encryption_table(self):
        self.encryption_view.viewport().update()
    
    def _on_checkbox_show_clicked(self):
        checked = self.checkbox_show_table.isChecked()
        self.encryption_view.setVisible(checked)
    
    def _on_button_load_clicked(self):
        filepath, _ = QtWidgets.QFileDialog.getOpenFileName(self, 'Load encryption table', '.', 'JSON Files (*.json *.JSON)')

        if filepath != '':
            self.application.load_encryption_table(filepath)
    
    def _on_button_save_clicked(self):
        filepath, _ = QtWidgets.QFileDialog.getSaveFileName(self, 'Save encryption table', '.', 'JSON Files (*.json *.JSON)')

        if filepath != '':
            self.application.save_encryption_table(filepath)
    
    def _on_button_random_clicked(self):
        self.application.generate_random_encryption_table()
