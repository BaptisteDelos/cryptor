from PySide2 import QtCore


class QCryptorModel(QtCore.QAbstractTableModel):
    """
    Defines a Qt model to manage a row of encoded numerals.

    This class is intended to be associated to a ConversionView.
    """

    def __init__(self, application, parent=None):
        QtCore.QAbstractTableModel.__init__(self, parent)
        self.application = application

    def rowCount(self, parent=None):
        return 1

    def columnCount(self, parent=None):
        return self.application.cryptor_cell_count()
    
    def headerData(self, section, orientation, role):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return self.application.cryptor_cell_header(section)
        
        return None

    def data(self, index, role):
        cell = self.application.cryptor_cell(index.column())

        if role == QtCore.Qt.UserRole:
            return cell

    def setData(self, index, value, role):
        if role == QtCore.Qt.EditRole:
            self.application.update_cryptor_cell(index.column(), value)
            self.dataChanged.emit(index, index)
            return True
        
        return False

    def flags(self, index):
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEditable


class QEncryptionModel(QtCore.QAbstractTableModel):
    """
    Defines a Qt model to manage a character encryption table.

    This class is intended to be associated to an EncryptionTableView.
    """

    def __init__(self, application, parent=None):
        QtCore.QAbstractTableModel.__init__(self, parent)
        self.application = application

    def rowCount(self, parent=None):
        return self.application.model_info()[0]

    def columnCount(self, parent=None):
        return self.application.model_info()[1]

    def data(self, index, role):
        cell = self.application.encryption_cell(index.row(), index.column())

        if role == QtCore.Qt.UserRole:
            return cell

    def setData(self, index, value, role):
        if role == QtCore.Qt.EditRole:
            self.application.update_encryption_cell(index.row(), index.column(), value[:1])
            self.dataChanged.emit(index, index)
            return True
        
        return False

    def flags(self, index):
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEditable
