from PySide2 import QtCore, QtGui, QtWidgets


# Conversion constants
MIN_CONVERSION_WIDTH = 100
PADDING = 20
CONVERSION_HEIGHT = 20

# Encryption constants
ORIGIN_SIZE = 20
MIN_ENCRYPTION_WIDTH = 20
MIN_ENCRYPTION_HEIGHT = 60
HOVER_WIDTH = 3


class CryptorDelegate(QtWidgets.QStyledItemDelegate):
    """
    Defines a delegate responsible for displaying conversion results.
    """

    def __init__(self, parent=None):
        QtWidgets.QStyledItemDelegate.__init__(self, parent)

        self.font = QtGui.QFont()
        self.metrics = QtGui.QFontMetrics(self.font)
    
    def paint(self, painter, option, index):
        cell = index.data(QtCore.Qt.UserRole)

        # Define draw areas
        rect_text = QtCore.QRect(
            option.rect.left() + PADDING / 2,
            option.rect.top() + PADDING / 2,
            option.rect.width() - PADDING,
            option.rect.height() - PADDING
        )

        painter.drawText(
            rect_text,
            QtCore.Qt.TextWordWrap | QtCore.Qt.AlignCenter,
            cell.text
        )

    def setEditorData(self, editor, index):
        cell = index.data(QtCore.Qt.UserRole)
        editor.setText(cell.text)
    
    def sizeHint(self, option, index):
        cell = index.data(QtCore.Qt.UserRole)
        cell_width = max(MIN_CONVERSION_WIDTH, self.metrics.width(cell.text) + PADDING)
        return QtCore.QSize(100, CONVERSION_HEIGHT)


class EncryptionDelegate(QtWidgets.QStyledItemDelegate):
    """
    Defines a delegate responsible for displaying character correspondences in an encryption table.
    """

    def __init__(self, parent=None):
        QtWidgets.QStyledItemDelegate.__init__(self, parent)

        self.font = QtGui.QFont("Courier",  10)
    
    def paint(self, painter, option, index):
        cell = index.data(QtCore.Qt.UserRole)
        draw_color = QtGui.QColor(43, 123, 158)

        # Define draw areas
        rect_origin = QtCore.QRect(
            option.rect.left() + 5,
            option.rect.top() + 5,
            ORIGIN_SIZE * 3,
            ORIGIN_SIZE
        )

        rect_substitution = QtCore.QRect(
            option.rect.left(),
            option.rect.top() + ORIGIN_SIZE,
            option.rect.width(),
            option.rect.height() - ORIGIN_SIZE
        )

        rect_hover = QtCore.QRect(
            option.rect.left(),
            option.rect.bottom() - HOVER_WIDTH,
            option.rect.width(),
            HOVER_WIDTH
        )

        # Draw covered cell rectangle
        if option.state & QtWidgets.QStyle.State_MouseOver:
            painter.setBrush(draw_color)
            painter.setPen(draw_color)
            painter.drawRect(rect_hover)

        # Draw original character
        painter.setBrush(QtCore.Qt.NoBrush)
        painter.setPen(draw_color)
        painter.setFont(self.font)

        if cell.origin == ' ':
            origin_text = 'space'
        else:
            origin_text = cell.origin

        painter.drawText(
            rect_origin,
            QtCore.Qt.AlignLeft,
            origin_text
        )

        # Draw substitution character
        painter.setPen(QtCore.Qt.black)
        painter.font().setWeight(12)

        painter.drawText(
            option.rect,
            QtCore.Qt.AlignCenter,
            cell.substitution
        )

    def setEditorData(self, editor, index):
        cell = index.data(QtCore.Qt.UserRole)
        editor.setText(cell.substitution)
    
    def sizeHint(self, option, index):
        return QtCore.QSize(MIN_ENCRYPTION_WIDTH, MIN_ENCRYPTION_HEIGHT)
