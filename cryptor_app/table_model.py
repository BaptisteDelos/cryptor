import string
import random

from cryptor_app.cell import CryptorCell, EncryptionCell


def to_string(value, col, data):
    if col != 0:
        value = "".join(value.split())
    string_value = ""

    if col == 1:
        for i in range(0, len(value), 8):
            element = value[i:i+8]
            string_value = string_value + chr(int(element, 2))
    elif col == 2:
        for character in value:
            for list_elem in data:
                for elem in list_elem:
                    if elem.substitution == character:
                        string_value = string_value + elem.origin
    elif col == 3:
        for i in range(0, len(value), 2):
            element = value[i:i+2]
            string_value = string_value + chr(int(element, 16))
    elif col == 4:
        for i in range(0, len(value), 3):
            element = value[i:i+3]
            string_value = string_value + chr(int(element, 8))
    else:
        string_value = value

    return string_value


def to_binary(value):
    binary_value = ' '.join(bin(ord(x))[2:].zfill(8) for x in value)
    return binary_value


def to_custom(value, data):
    custom_value = ''

    for character in value:
        for list_elem in data:
            for elem in list_elem:
                if elem.origin == character:
                    custom_value = custom_value + elem.substitution

    return custom_value


def to_hexadecimal(value):
    hexadecimal_value = ' '.join(hex(ord(x))[2:] for x in value)
    return hexadecimal_value


def to_octal(value):
    octal_value = ' '.join(oct(ord(x))[2:] for x in value)
    return octal_value


class CryptorTable:

    def __init__(self):
        self.data = [CryptorCell('')] * 5

    def update_cell(self, col, value, data):
        string_value = to_string(value, col, data)

        self.data[0] = CryptorCell(string_value)
        self.data[1] = CryptorCell(to_binary(string_value))
        self.data[2] = CryptorCell(to_custom(string_value, data))
        self.data[3] = CryptorCell(to_hexadecimal(string_value))
        self.data[4] = CryptorCell(to_octal(string_value))


class EncryptionTable:

    def __init__(self, col):
        self.data = list()
        self.init_table(col)

    def init_table(self, col):
        self.characters = string.ascii_letters + string.digits + string.punctuation + ' '
        table = [self.characters[i:i+col] for i in range(0, len(self.characters), col)]

        for row in table:
            self.data.append([EncryptionCell(character, '') for character in row])

    def info(self):
        row_count = len(self.data)
        column_count = max(len(item) for item in self.data)
        return row_count, column_count

    def update_cell(self, row, col, value):
        self.data[row][col] = EncryptionCell(self.data[row][col].origin, value)

    def load_table(self, load_data):
        for row, list_elem in enumerate(self.data):
            for col, elem in enumerate(list_elem):
                if elem.origin in load_data:
                    self.update_cell(row, col, load_data[elem.origin])

    def random_cell(self):
        characters_list = list(self.characters)
        random.shuffle(characters_list)
        characters_iter = iter(characters_list)

        for row, list_elem in enumerate(self.data):
            for col, elem in enumerate(list_elem):
                self.update_cell(row, col, next(characters_iter))
