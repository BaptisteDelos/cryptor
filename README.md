     __ .__ .   ,.__ .___..__..__
    /  `[__) \./ [__)  |  |  |[__)
    \__.|  \  |  |     |  |__||  \


**Table Model/View Encryption app**
<br>

---

Encoding options :
- UNICODE
- BINARY
- CUSTOM

Optional :
- HEXADECIMAL
- OCTAL

<br>

---

TABLE VIEW

| UNICODE |  BINARY  | CUSTOM | HEXADECIMAL | OCTAL |
| ------- | -------- | ------ | ----------- | ----- |
|   203   | 11001011 |   iK0  |      CB     |  313  |

<br>

---

Encryption table for CUSTOM

| A | B | C | D | E | F | G | H | I | J | K | L | M | N | O | P | Q | R | S | T | U | V | W | X | Y | Z | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |
| - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - |
| t | n | ! | f | E | s | 2 | 9 | e | 1 | K | L | M | q | O | x | Q | R | S | T | U | z | W | E | Y | Z | K | P | i | 0 | 4 | 5 | 6 | 7 | 3 | 9 |


 <br><br><br>

 ---

**Links :**

> https://gitlab.com/BaptisteDelos/cryptor

> https://codecollab.io/@proj/CompetitionSunsetApple
